import React from 'react';
import "../Styles/splash.css";

const Splash = ({onLogin}) => {
    const [email, setEmail] = React.useState('');
    const [password, setPassword] = React.useState('');

    return (
        <div className="login">
            <label>
                Email:
                <input
                    name='email'
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                />
            </label>
            <label>
                Password:
                <input
                    name='password'
                    type='password'
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                />
            </label>
            <div>
                <button onClick={() => onLogin({ email, password })}>Zaloguj</button>
            </div>
        </div>
    );

};

export default Splash;