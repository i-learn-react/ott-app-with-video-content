import { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

class Portal extends Component {
    constructor(props) {
        super(props);
        this.container = document.createElement('div');
        this.parentNode = document.querySelector(this.props.in);
    }

    componentDidMount() {
        if (this.props.firstElement) {
            this.parentNode.prepend(this.container);
        } else {
            this.parentNode.append(this.container);
        }
    }

    componentWillUnmount() {
        this.parentNode.removeChild(this.container);
    }
    
    
    render() {
        return (
            ReactDOM.createPortal(this.props.children, this.container)
        );
    }
}

Portal.propTypes = {
    in: PropTypes.string,
    children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
    firstElement: PropTypes.bool,
};

Portal.defaultProps = {
    in: 'body',
    firstElement: false,
}

export default Portal;
