import React, { Component } from 'react';

import { AuthenticationApi, getAccessToken } from '../Utils/Authentication';
import Dashboard from './Dashboard';
import Splash from './Splash';

class Main extends Component {
    state = {
        accessToken: null,
        errorMessage: '',
        hasError: false,
    }

    catchError = error =>
        this.setState({ hasError: true, errorMessage: error.Message });

    handleLogin = (credentials) =>
        AuthenticationApi
            .login(credentials)
            .then(getAccessToken)
            .then((accessToken) => {
                this.setState({
                    hasError: false,
                    errorMessage: '',
                    accessToken,
                });
            })
            .catch(this.catchError);

    render() {
        const { accessToken } = this.state;
        return (
            accessToken ? <Dashboard accessToken={accessToken} /> : <Splash onLogin={this.handleLogin}/>
        );
    }
}

export default Main;