import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Columns} from 'react-bulma-components';
import { MediaApi } from '../Utils/MediaApi';
import Media from '../Models/Media';
import MediaContainer from './MediaContainer';


class Dashboard extends Component {
    state = {
        data: [],
    }

    catchError = error =>
        this.setState({ hasError: true, errorMessage: error.Message });

    getMediaList = (accessToken) =>
        MediaApi
            .getMediaList(accessToken)
            .then(({Entities}) => {
                this.setState({
                    hasError: false,
                    errorMessage: '',
                    data: Entities.map(item => new Media(item)),
                });
            })
            .catch(this.catchError);
    
    componentDidMount() {
        this.getMediaList(this.props.accessToken)    
    }
    
    render() {
        const { data } = this.state;
        return (
            <Columns centered>
                <Columns.Column size="half">
                    {data.map(video =>
                        <MediaContainer key={video.guid} video={video} accessToken={this.props.accessToken}/>
                    )}
                </Columns.Column>
            </Columns>
        );
    }
}

Dashboard.propTypes = {
    accessToken: PropTypes.string.isRequired,
};

export default Dashboard;