import React, { Fragment } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {Box, Card, Heading, Content} from 'react-bulma-components';
import MediaPlayer from './MediaPlayer';

const MediaContainer = ({ video, accessToken }) => {
    const [videoId, setVideoId] = React.useState(null);
    return (
        <Fragment>
            <Box key={video.guid}>
                <Card onClick={(() => setVideoId(video.id))}>
                    {video.images.map(image => <Card.Image key={image.guid} src={image.url} />)}
                    <Card.Content>
                        <Heading size={4}>{video.title}</Heading>
                        <Content>
                            {video.description}
                        </Content>
                    </Card.Content>
                </Card>
            </Box>
            {videoId && <MediaPlayer videoId={videoId} accessToken={accessToken} />}
        </Fragment>
    );
};

export default MediaContainer;