import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactPlayer from 'react-player';
import Portal from './Portal';
import { MediaApi } from '../Utils/MediaApi';
import '../Styles/modalDialog.css';

class MediaPlayer extends Component {
    state = {
        url: null,
        hasError: false,
        errorMessage: '',
    };

    onClose = () => this.setState({ url: null });
    catchError = error =>
        this.setState({ hasError: true, errorMessage: error.Message });

    getMediaPlayInfo = (id, accessToken) =>
        MediaApi
            .getMediaPlayInfo(id, accessToken)
            .then(({ ContentUrl }) => {
                this.setState({
                    hasError: false,
                    errorMessage: '',
                    url: ContentUrl,
                });
            })
            .catch(this.catchError);
    
    componentDidMount() {
        this.getMediaPlayInfo(this.props.videoId, this.props.accessToken)    
    }
    
    componentWillUnmount() {
        this.destroy();
    }
    
    render() {
        const { url } = this.state;
        return (
            url && (
                <Portal>
                    <div className='backdrop'>
                        <div className='modal-window'>
                            <button onClick={this.onClose}>Close</button>
                            <ReactPlayer url={url} controls />
                        </div>
                    </div>
                </Portal>
            )
        );
    }
}

MediaPlayer.propTypes = {
    videoId: PropTypes.string,
    accessToken: PropTypes.string,
};

export default MediaPlayer;