import { BASE_URL } from "../constants";
import { makeApiRequest } from "./makeApiRequest";

export const MediaApi = {};

const defaultMediaListBody = {
    "MediaListId": 3,
    "IncludeCategories": false,
    "IncludeImages": true,
    "IncludeMedia": false,
    "PageNumber": 1,
    "PageSize": 10
};

MediaApi.getMediaList = async function (accessToken) {
    return await makeApiRequest(`${BASE_URL}/Media/GetMediaList`, 'POST', defaultMediaListBody, accessToken);
}

MediaApi.getMediaPlayInfo = async function (id, accessToken) {
    return await makeApiRequest(`${BASE_URL}/Media/GetMediaPlayInfo?mediaId=${id}`, 'GET', null, accessToken);
}