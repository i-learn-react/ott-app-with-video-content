import { BASE_URL } from "../constants";
import { makeApiRequest } from "./makeApiRequest";

export const AuthenticationApi = {};

AuthenticationApi.login = async function (credentials) {
    return await makeApiRequest(`${BASE_URL}/Authorization/SignIn`, 'POST', credentials);
}

export const getAccessToken = (response) => response.Result.AuthorizationToken.Token;
