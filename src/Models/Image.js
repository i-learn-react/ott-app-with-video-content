class Image {
    constructor({ Guid, Id, MediaId, Url, }) {
        this.guid = Guid;
        this.id = Id;
        this.mediaId = MediaId;
        this.url = Url;
    }
}

export default Image;