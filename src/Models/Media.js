import Image from './Image';

const getFrameImages = (images) =>
    images
        .filter(image => image.ImageTypeCode === "FRAME")
        .map(image => new Image(image));
        
class Media {
    constructor({ Guid, Id, Title, Description, Images }) {
        this.guid = Guid;
        this.id = Id;
        this.title = Title;
        this.description = Description;
        this.images = getFrameImages(Images);
    }
}

export default Media;